package ictgradschool;

import ictgradschool.industry.lab04.ex03.GuessingGame;
import ictgradschool.industry.lab04.ex04.RockPaperScissors;
import ictgradschool.industry.lab04.ex05.PrintPatternProgram;
import ictgradschool.industry.lab04.ex06.DisplayMobilePrices;
import ictgradschool.industry.lab04.ex07.LecturerProgram;
import ictgradschool.industry.lab04.ex08.Movie;
import ictgradschool.industry.lab04.ex08.MovieProgram;

/**
 * Entry point of the program.
 * */
public class RunMe {
    public static void main(String[] args) {
        if (args.length == 0) {
            System.out.println("Ergh");
            System.exit(-1);
        }

        if (args[0].equals("3")) {
            GuessingGame g = new GuessingGame();
            g.start();
        } else if (args[0].equals("4")) {
            RockPaperScissors r = new RockPaperScissors();
            r.start();
        } else if (args[0].equals("5")) {
            PrintPatternProgram.main(args);
        } else if (args[0].equals("6")) {
            DisplayMobilePrices.main(args);
        } else if (args[0].equals("7")) {
            LecturerProgram.main(args);
        } else if (args[0].equals("8")) {
            MovieProgram.main(args);
        }
    }
}

package ictgradschool.industry.lab04.ex05;

public class Pattern {
    private int numChars;
    private char character;

    Pattern(int numChars, char character){
        this.numChars = numChars;
        this.character = character;
    }

    void setNumberOfCharacters(int numChars){
        this.numChars = numChars;
    }

    int getNumberOfCharacters(){
        return numChars;
    }

    public String toString(){
        StringBuilder res = new StringBuilder();
        for (int i = 0; i < numChars; i++) {
            res.append(character);
        }
        return res.toString();
    }
}

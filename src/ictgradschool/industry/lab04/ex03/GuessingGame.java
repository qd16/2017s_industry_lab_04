package ictgradschool.industry.lab04.ex03;

import ictgradschool.Keyboard;

import java.util.Random;

/**
 * A guessing game!
 */
public class GuessingGame {

    public void start() {

        // TODO Write your code here.
        Random rand = new Random();
        int randomNumber = rand.nextInt(100) + 1;
        int guess = 0;
        while (guess != randomNumber){
            System.out.print("Enter your guess (1-100): ");
            try {
                guess = Integer.parseInt(Keyboard.readInput());
            } catch (Exception e){
                System.out.println("Please enter a valid integer number!");
            }
            if (guess < randomNumber)
                System.out.println("Too low, try again");
            else if (guess > randomNumber)
                System.out.println("Too high, try again");
            else {
                System.out.println("Perfect!");
                break;
            }
        }
        System.out.println("Goodbye");
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        GuessingGame ex = new GuessingGame();
        ex.start();

    }
}

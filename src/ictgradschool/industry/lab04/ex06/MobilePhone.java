package ictgradschool.industry.lab04.ex06;

public class MobilePhone {

    // TODO Declare the 3 instance variables:
     String brand;
     String model;
     double price;
    
    public MobilePhone(String brand, String model, double price) {
        // Complete this constructor method
        this.brand = brand;
        this.model = model;
        this.price = price;
    }

    // TODO Uncomment these methods once the corresponding instance variable has been declared.
    public String getBrand() {
        return brand;
    }
    
    public void setBrand(String brand) {
        this.brand = brand;
    }
    
    // TODO Insert getModel() method here
    public String getModel() {
        return model;
    }

    // TODO Insert setModel() method here
    public void setModel(String model) {
        this.model = model;
    }

    // TODO Insert getPrice() method here

    public double getPrice() {
        return price;
    }


    // TODO Insert setPrice() method here

    public void setPrice(double price) {
        this.price = price;
    }


    // TODO Insert toString() method here
    public String toString(){
        return String.format("%s %s which cost $%.2f", brand, model, price);
    }

    
    // TODO Insert isCheaperThan() method here
    public boolean isCheaperThan(MobilePhone other){
        return this.price < other.price;
    }
    
    // TODO Insert equals() method here
    public boolean equals(Object other){
        if (!(other instanceof MobilePhone))
            return false;

        return this.brand.equals(((MobilePhone)other).brand) && this.model.equals(((MobilePhone)other).model);
    }

}



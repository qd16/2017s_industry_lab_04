package ictgradschool.industry.lab04.ex04;

import ictgradschool.Keyboard;

import java.net.Inet4Address;
import java.util.Random;

/**
 A game of Rock, Paper Scissors
 */
public class RockPaperScissors {

    // TODO Make similar constants for PAPER and SCISSORS, to improve readability of your code.
    public static final int ROCK = 1;
    public static final int SCISSORS = 2;
    public static final int PAPER = 3;

    public void start() {

        // TODO Write your code here which calls your other methods in order to play the game. Implement this
        // as detailed in the exercise sheet.
        System.out.println("Hi! What is your name? ");
        String playerName = Keyboard.readInput();
        int choice = 0;
        while (true){
            System.out.print("\n 1. Rock\n 2. Scissors\n 3. Paper\n 4. Quit\n Enter choice: ");
            do{
                try {
                    choice = Integer.parseInt(Keyboard.readInput());
                } catch (Exception e) {
                    System.out.println("Please enter a valid choice above!");
                }
                finally {
                    if (choice < 1 || choice > 4)
                        System.out.println("Please enter a valid choice above!");
                }
            } while (choice < 1 || choice > 4);

            if (choice == 4)
                break;
            else{
                Random rand = new Random();
                int computerChoice = rand.nextInt(3) + 1;
                displayPlayerChoice(playerName, choice);
                displayPlayerChoice("Computer", computerChoice);
                String winner = userWins(choice, computerChoice) ? playerName : "Computer";
                if (choice == computerChoice)
                    System.out.print("No one wins. Because ");
                else
                    System.out.print(String.format("%s wins. Because ", userWins(choice, computerChoice)? playerName : "The computer"));
                System.out.println(getResultString(choice, computerChoice));
            }
        }

        System.out.println(String.format("Goodbye %s. Thanks for playing :)", playerName));

    }


    public void displayPlayerChoice(String name, int choice) {
        // TODO This method should print out a message stating that someone chose a particular thing (rock, paper or scissors)
        String chosenItem = "";
        switch (choice){
            case 1:
                chosenItem = "rock";
                break;
            case 2:
                chosenItem = "scissors";
                break;
            case 3:
                chosenItem = "paper";
                break;
        }
        System.out.println(String.format("%s chose %s.", name, chosenItem));
    }

    public boolean userWins(int playerChoice, int computerChoice) {
        // TODO Determine who wins and return true if the player won, false otherwise.
        return (playerChoice == 1 && computerChoice == 2
                || playerChoice == 2 && computerChoice == 3
                || playerChoice == 3 && computerChoice == 1);
    }

    public String getResultString(int playerChoice, int computerChoice) {

        final String PAPER_WINS = "paper covers rock";
        final String ROCK_WINS = "rock smashes scissors";
        final String SCISSORS_WINS = "scissors cut paper";
        final String TIE = "you chose the same as the computer";

        // TODO Return one of the above messages depending on what playerChoice and computerChoice are.
        if (playerChoice == 1 && computerChoice == 2 || playerChoice == 2 && computerChoice == 1)
            return ROCK_WINS;
        else if (playerChoice == 2 && computerChoice == 3 || playerChoice == 3 && computerChoice == 2)
            return SCISSORS_WINS;
        else if (playerChoice == 3 && computerChoice == 1 || playerChoice == 1 && computerChoice == 3)
            return PAPER_WINS;
        else
        return TIE;
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        RockPaperScissors ex = new RockPaperScissors();
        ex.start();

    }
}
